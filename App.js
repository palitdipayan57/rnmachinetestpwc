/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import axios from 'axios';
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  useColorScheme,
  View,
  Image,
  FlatList
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      movieList: [],
      text: ''
    }
  }


  renderItem(item, index) {
    return (
      <View
        style={{
          width: wp(90),
          height: wp(20),
          backgroundColor: 'white',
          marginBottom: wp(3),
          borderRadius: wp(2),
          borderWidth: 0.5,
          borderColor: 'gray',
          justifyContent: 'center',
          paddingLeft: wp(3),
          paddingRight: wp(3)
        }}>
        <Text
          style={{
            fontSize: wp(4),
            color: 'black',
            fontWeight: 'bold'
          }}>
          Movie Name
        </Text>
        <Text
          numberOfLines={2}
          style={{
            fontSize: wp(3.5),
            color: 'black',
            fontWeight: '500',
            marginTop: wp(1)
          }}>
          Movie Des
        </Text>
      </View>

    )


  }

  async fetchMovieList() {

    await axios.get('https://gateway.marvel.com/v1/public/characters?nameStartsWith=' + this.state.text + '&apikey=6871af630ca51742153d2db8dbf10dcb&hash=609bb5211dbf0e54c53e927bb92f5ee7&ts=1')
      .then(res => {
        console.log('list', res);
      }).catch(err => {
        console.log('err', err);
      })

  }

  render() {
    return (
      <View
        style={styles.mainContainer}>
        <View
          style={styles.textInputView}>
          <TextInput
            placeholder={'Search your movie name...'}
            style={styles.textInput}
            value={this.state.text}
            onChangeText={(txt) => this.setState({ text: txt }, () => {
              this.fetchMovieList()
            })}
          />
          <Image
            source={require('./src/assets/search.png')}
            style={{
              width: wp(5),
              height: wp(5),
              resizeMode: 'contain'
            }}
          />
        </View>
        {this.state.movieList.length > 0 ?
          <FlatList
            style={{ marginTop: wp(5) }}
            data={this.state.movieList}
            renderItem={({ item, index }) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
          /> : null}
      </View>
    )
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center'
  },
  textInput: {
    width: wp(75),
    height: wp(10),
    paddingLeft: wp(1),
    paddingRight: wp(1),
    backgroundColor: 'white'
  },
  textInputView: {
    width: wp(90),
    height: wp(12),
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: wp(2),
    marginTop: wp(5),
    borderRadius: wp(2),
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: 'gray',
    elevation: 2
  }
});

export default App;
